//
//  MapViewController.swift
//  MapSight
//
//  Created by ladmin on 13.07.2020.
//  Copyright © 2020 Areas. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate  {
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        mapView.delegate = self
        locationManager.startUpdatingLocation()
//        mapView.showsUserLocation = true
        mapView.userLocation.title = "I'm"
        mapView.userLocation.subtitle = "Subtitle"
        // Do any additional setup after loading the view.
        
        let myFirstAnnotation = MKPointAnnotation()
        myFirstAnnotation.title = "Pet Club"
        myFirstAnnotation.coordinate = CLLocationCoordinate2D(latitude: 51.50960, longitude: -0.1337)
        mapView.addAnnotation(myFirstAnnotation)
    }
    
    func mapToCoordinate(coordinate: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last?.coordinate {
            mapToCoordinate(coordinate: location)
            //        locationManager.stopUpdatingLocation()
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.coordinate.latitude != mapView.userLocation.coordinate.latitude && annotation.coordinate.longitude != mapView.userLocation.coordinate.longitude {
            let marker = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "marker")

            marker.canShowCallout = true
            let infoButton = UIButton(type: .detailDisclosure)
            infoButton.addTarget(self, action: #selector(infoAction), for: .touchUpInside)
            marker.rightCalloutAccessoryView = infoButton
            marker.calloutOffset = CGPoint(x: -5, y: 5)
            return marker
        }
        return nil
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print(view.annotation?.title)
    }

    @objc func infoAction() {
        print("Info")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
