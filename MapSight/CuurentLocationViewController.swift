//
//  ViewController.swift
//  MapSight
//
//  Created by ladmin on 13.07.2020.
//  Copyright © 2020 Areas. All rights reserved.
//

import UIKit
import CoreLocation

class CuurentLocationViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkAuthorization()
    }
    
    func checkAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        case .denied:
            //            let alert = UIAlertController(title: "Sorry", message: "Вы не можете пользоваться приложением", preferredStyle: .alert)
            //            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            //            alert.addAction(ok)
            
            print("Error")
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            break
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last?.coordinate {
            latitudeLabel.text = String(location.latitude)
            longitudeLabel.text = String(location.longitude)
            locationManager.stopUpdatingLocation()
        }
    }
    
}

